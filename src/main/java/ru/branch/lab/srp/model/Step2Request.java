package ru.branch.lab.srp.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.math.BigInteger;

@Data
@AllArgsConstructor
@Setter
@Getter
public class Step2Request {

    // Передаётся со стороны клиента
    private BigInteger A;


    // Поля ниже обогащаются из хранилища (кэша)
    private BigInteger M1;
    private BigInteger B;
    private BigInteger privateB;
    private BigInteger verifier;
}
