package ru.branch.lab.srp.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.math.BigInteger;

@Data
@AllArgsConstructor
@Setter
@Getter
public class Step1Request {
    private String login;
    private BigInteger salt;
    private BigInteger verifier;
}
