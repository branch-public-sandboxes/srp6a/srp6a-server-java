package ru.branch.lab.srp.exception;

public class SRP6Exception extends Exception {
    private static final long serialVersionUID = 4640494990301260666L;

    /**
     * SRP-6a exception causes.
     */
    public enum CauseType {

        /**
         * Invalid public client or server value ('A' or 'B').
         */
        BAD_PUBLIC_VALUE,

        /**
         * Invalid credentials (password).
         */
        BAD_CREDENTIALS
    }


    /**
     * The cause type.
     */
    private CauseType cause;

    public SRP6Exception(final String message, final CauseType cause) {
        super(message);
        if (cause == null)
            throw new IllegalArgumentException("The cause type must not be null");
        this.cause = cause;
    }
}
