package ru.branch.lab.srp.core;

import java.io.Serializable;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.SecureRandom;

public class SRP6Routines implements Serializable {
    protected SecureRandom random = new SecureRandom();

    public BigInteger computeK(MessageDigest digest, BigInteger N, BigInteger g) {
        return this.hashPaddedPair(digest, N, N, g);
    }

    public byte[] generateRandomSalt(int numBytes) {
        return this.generateRandomSalt(numBytes, this.random);
    }

    public byte[] generateRandomSalt(int numBytes, SecureRandom random) {
        byte[] salt = new byte[numBytes];
        random.nextBytes(salt);
        return salt;
    }

    public BigInteger computeX(MessageDigest digest, byte[] salt, byte[] username, byte[] password) {
        digest.update(username);
        digest.update((byte)58);
        digest.update(password);
        byte[] output = digest.digest();
        digest.update(salt);
        output = digest.digest(output);
        return BigIntegerUtils.bigIntegerFromBytes(output);
    }

    public BigInteger computeVerifier(BigInteger N, BigInteger g, BigInteger x) {
        return g.modPow(x, N);
    }

    public BigInteger generatePrivateValue(BigInteger N, SecureRandom random) {
        int minBits = Math.max(256, N.bitLength());

        BigInteger r;
        for(r = BigInteger.ZERO; BigInteger.ZERO.equals(r); r = (new BigInteger(minBits, random)).mod(N)) {
        }

        return r;
    }

    public BigInteger computePublicServerValue(BigInteger N, BigInteger g, BigInteger k, BigInteger v, BigInteger b) {
        return g.modPow(b, N).add(v.multiply(k)).mod(N);
    }

    public boolean isValidPublicValue(BigInteger N, BigInteger value) {
        return !value.mod(N).equals(BigInteger.ZERO);
    }

    public BigInteger computeU(MessageDigest digest, BigInteger N, BigInteger A, BigInteger B) {
        return this.hashPaddedPair(digest, N, A, B);
    }

    public BigInteger computeSessionKey(BigInteger N, BigInteger v, BigInteger u, BigInteger A, BigInteger b) {
        return v.modPow(u, N).multiply(A).modPow(b, N);
    }

    public BigInteger computeClientEvidence(MessageDigest digest, BigInteger A, BigInteger B, BigInteger S) {
        digest.update(BigIntegerUtils.bigIntegerToBytes(A));
        digest.update(BigIntegerUtils.bigIntegerToBytes(B));
        digest.update(BigIntegerUtils.bigIntegerToBytes(S));
        return BigIntegerUtils.bigIntegerFromBytes(digest.digest());
    }

    protected BigInteger computeServerEvidence(MessageDigest digest, BigInteger A, BigInteger M1, BigInteger S) {
        digest.update(BigIntegerUtils.bigIntegerToBytes(A));
        digest.update(BigIntegerUtils.bigIntegerToBytes(M1));
        digest.update(BigIntegerUtils.bigIntegerToBytes(S));
        return BigIntegerUtils.bigIntegerFromBytes(digest.digest());
    }

    protected BigInteger hashPaddedPair(MessageDigest digest, BigInteger N, BigInteger n1, BigInteger n2) {
        int padLength = (N.bitLength() + 7) / 8;
        byte[] n1_bytes = this.getPadded(n1, padLength);
        byte[] n2_bytes = this.getPadded(n2, padLength);
        digest.update(n1_bytes);
        digest.update(n2_bytes);
        byte[] output = digest.digest();
        return BigIntegerUtils.bigIntegerFromBytes(output);
    }

    protected byte[] getPadded(BigInteger n, int length) {
        byte[] bs = BigIntegerUtils.bigIntegerToBytes(n);
        if (bs.length < length) {
            byte[] tmp = new byte[length];
            System.arraycopy(bs, 0, tmp, length - bs.length, bs.length);
            bs = tmp;
        }

        return bs;
    }

    public SRP6Routines() {
    }
}
