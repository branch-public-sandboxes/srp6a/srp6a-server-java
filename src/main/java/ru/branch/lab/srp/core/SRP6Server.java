package ru.branch.lab.srp.core;

import ru.branch.lab.srp.exception.SRP6Exception;
import ru.branch.lab.srp.model.Step1Request;
import ru.branch.lab.srp.model.Step1Response;
import ru.branch.lab.srp.model.Step2Request;
import ru.branch.lab.srp.model.Step2Response;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.SecureRandom;

/**
 * TODO: Из логики удалены:
 * 1. Проверка таймаутов между шагами
 * 2. Проверка этапа авторизации
 */
public class SRP6Server {

    private SRP6CryptoParams config;
    private SRP6Routines srp6Routines;

    public SRP6Server(SRP6CryptoParams config) {
        this.config = config;
        this.srp6Routines = new SRP6Routines();
    }

    /**
     * Шаг 1 - Расчёт первой пары публичных ключей
     * <p>
     * На входе получаем: логин, верификатор (v), соль (s)
     * На выходе получаем:
     * ПУБЛИЧНО: B (публичный ключ), соль (s)
     * ДЛЯ ХРАНЕНИЯ: b (приватный ключ), верификатор (v)
     *
     * @param step1Request - Обёртка с логином, солью и верификатором
     */
    public Step1Response step1(final Step1Request step1Request) {
        String userID = step1Request.getLogin();
        BigInteger s = step1Request.getSalt();
        BigInteger v = step1Request.getVerifier();

        SecureRandom random = new SecureRandom();
        if (userID != null && !userID.trim().isEmpty()) {
            if (s == null) {
                throw new IllegalArgumentException("The salt 's' must not be null");
            } else {
                if (v == null) {
                    throw new IllegalArgumentException("The verifier 'v' must not be null");
                } else {
                    MessageDigest digest = config.getMessageDigestInstance();
                    BigInteger k = srp6Routines.computeK(digest, config.N, config.g);
                    digest.reset();
                    BigInteger b = srp6Routines.generatePrivateValue(config.N, random);
                    digest.reset();
                    BigInteger B = this.srp6Routines.computePublicServerValue(config.N, config.g, k, v, b);
                    return new Step1Response(B, b);
                }
            }
        } else {
            throw new IllegalArgumentException("The user identity 'I' must not be null or empty");
        }
    }

    /**
     * Шаг 2 - Считаем M2 и сравниваем с M1 (от клиента)ю
     * <p>
     * На входе получаем: A (публичный клиентский ключ), M1
     * На выходе получаем:
     */
    public Step2Response step2(final Step2Request step2Request) throws SRP6Exception {
        BigInteger A = step2Request.getA();
        if (A == null)
            throw new IllegalArgumentException("The client public value 'A' must not be null");

        BigInteger B = step2Request.getB();
        if (B == null)
            throw new IllegalArgumentException("The server public value 'B' must not be null");

        BigInteger v = step2Request.getVerifier();
        if (v == null)
            throw new IllegalArgumentException("The verifier value 'v' must not be null");

        BigInteger b = step2Request.getPrivateB();
        if (b == null)
            throw new IllegalArgumentException("The server private value 'b' must not be null");

        BigInteger M1 = step2Request.getM1();
        if (M1 == null)
            throw new IllegalArgumentException("The client evidence message 'M1' must not be null");

        if (!srp6Routines.isValidPublicValue(config.N, A))
            throw new SRP6Exception("Bad client public value 'A'", SRP6Exception.CauseType.BAD_PUBLIC_VALUE);

        MessageDigest digest = config.getMessageDigestInstance();

        BigInteger u = srp6Routines.computeU(digest, config.N, A, B);
        digest.reset();

        BigInteger Ss = srp6Routines.computeSessionKey(config.N, v, u, A, b);

        BigInteger computedM1 = srp6Routines.computeClientEvidence(digest, A, B, Ss);
        digest.reset();

        if (!computedM1.equals(M1))
            throw new SRP6Exception("Bad client credentials", SRP6Exception.CauseType.BAD_CREDENTIALS);

        BigInteger M2 = srp6Routines.computeServerEvidence(digest, A, M1, Ss);
        digest.reset();

        return new Step2Response(M2);
    }
}