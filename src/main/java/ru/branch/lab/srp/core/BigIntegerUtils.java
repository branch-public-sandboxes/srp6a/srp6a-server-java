package ru.branch.lab.srp.core;

import java.math.BigInteger;
import java.util.Arrays;

public class BigIntegerUtils {

    public static BigInteger fromHex(String hex) {
        if (hex == null) {
            return null;
        } else {
            try {
                return new BigInteger(hex, 16);
            } catch (NumberFormatException var2) {
                return null;
            }
        }
    }

    public static BigInteger bigIntegerFromBytes(byte[] bytes) {
        return new BigInteger(1, bytes);
    }

    public static byte[] bigIntegerToBytes(BigInteger bigInteger) {
        assert bigInteger.signum() != -1;

        byte[] bytes = bigInteger.toByteArray();
        return bytes[0] == 0 ? Arrays.copyOfRange(bytes, 1, bytes.length) : bytes;
    }

    private BigIntegerUtils() {
    }
}
